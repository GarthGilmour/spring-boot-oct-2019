# Spring Boot with Java

These are the examples and exercises for the October 2019 delivery of 'Spring Boot with Java'.

The manual for the course can be [downloaded from here](https://drive.google.com/file/d/1tyUzyLrSOSygLVRwFU0r4dWBX3GJMS8Y/view?usp=sharing).

