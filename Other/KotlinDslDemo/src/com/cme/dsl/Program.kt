package com.cme.dsl

class Module(private val title: String) {
    private val items = mutableListOf<String>()

    operator fun String.unaryPlus() {
        items.add(this)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("\tModule called '$title' with ${items.size} items\n")
        items.forEach { sb.append("\t\tItem called '$it'\n") }
        return sb.toString()
    }
}

class Course(private val title: String) {
    private val modules = mutableListOf<Module>()

    fun module(title: String, action: Module.() -> Unit): Unit {
        val theModule = Module(title)
        theModule.action()
        modules.add(theModule)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append("Course called '$title' with ${modules.size} modules\n")
        modules.forEach { sb.append(it) }
        return sb.toString()
    }
}

fun course(title: String, action: Course.() -> Unit): Course {
    val theCourse = Course(title)
    theCourse.action()
    return theCourse
}

fun main() {
    val dsl = course("Intro To Spring") {
        module("DI") {
            +"Using @Autowired"
            +"Using @Resource"
            +"Using @Value"
        }
        module("AOP") {
            +"Whats an Aspect?"
            +"Whats a Join Point"
            +"Whats a Pointcut?"
        }
    }
    println(dsl)
}
