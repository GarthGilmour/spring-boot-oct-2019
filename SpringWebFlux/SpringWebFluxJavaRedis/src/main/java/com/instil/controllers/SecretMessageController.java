package com.instil.controllers;

import org.springframework.data.redis.core.ReactiveRedisOperations;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("secret")
class SecretMessageController {
    private final ReactiveRedisOperations<String, String> stringOps;
    private String key = "SecretMessage";

    SecretMessageController(ReactiveRedisOperations<String, String> stringOps) {
        this.stringOps = stringOps;
    }

    // Example of getting by key
    @GetMapping
    public Mono<String> get() {
        return stringOps.opsForValue().get(key);
    }

    // Example of setting a value
    @PostMapping("/{message}")
    public Mono<Boolean> set(@PathVariable("message") String message) {
        return stringOps.opsForValue().set(key, message);
    }

    // Example of a short lived piece of data
    @PostMapping("/short/{message}")
    public Mono<Boolean> setShortLive(@PathVariable("message") String message) {
        return stringOps.opsForValue().set(key, message, Duration.ofSeconds(10));
    }

    // Deleting by key
    @DeleteMapping
    public Mono<Boolean> delete() {
        return stringOps.opsForValue().delete(key);
    }
}
