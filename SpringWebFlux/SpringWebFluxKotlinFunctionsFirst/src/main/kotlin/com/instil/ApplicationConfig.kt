package com.instil

import com.instil.model.Course
import com.instil.model.CourseDifficulty.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ApplicationConfig {
    @Bean(name = ["portfolio"])
    fun buildPortfolio() : MutableMap<String, Course> {
        val portfolio = mutableMapOf<String, Course>()
        portfolio["AB12"] = Course("AB12", "Programming in Scala", BEGINNER, 4)
        portfolio["CD34"] = Course("CD34", "Machine Learning in Python", INTERMEDIATE, 3)
        portfolio["EF56"] = Course("EF56", "Advanced Kotlin Coding", ADVANCED, 2)
        portfolio["GH78"] = Course("GH78", "Intro to Domain Driven Design", BEGINNER, 3)
        portfolio["IJ90"] = Course("IJ90", "Database Access with JPA", INTERMEDIATE, 3)
        portfolio["KL12"] = Course("KL12", "Functional Design Patterns in F#", ADVANCED, 2)
        portfolio["MN34"] = Course("MN34", "Building Web UIs with Angular", BEGINNER, 4)
        portfolio["OP56"] = Course("OP56", "Version Control with Git", INTERMEDIATE, 1)
        portfolio["QR78"] = Course("QR78", "SQL Server Masterclass", ADVANCED, 2)
        portfolio["ST90"] = Course("ST90", "Go Programming for Beginners", BEGINNER, 5)
        portfolio["UV12"] = Course("UV12", "Coding with Lock Free Algorithms", INTERMEDIATE, 2)
        portfolio["WX34"] = Course("WX34", "Coaching Skills for SCRUM Masters", ADVANCED, 3)

        return portfolio
    }
}
